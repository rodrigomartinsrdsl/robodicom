/**
 * 
 */
package br.com.rdsl.infomed.utils;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

/**
 * @author rodrigo.martins
 *
 */
public class DirectoryListerTest {
	
	private String getDefaultPath(){
		StringBuilder sb = new StringBuilder();
		sb.append(System.getProperty("user.home"));
		sb.append("/Documents/DICOMData/636/");
		
		return sb.toString();
	}
	
	@Test
	public void getFileFromDirectory() {
		List<Path> listOfFilePath = DirectoryLister.listFilesAtDirectory(getDefaultPath());
		
		Object expected = true;
		Object actual  = listOfFilePath.size() > 0;
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFileRecursivellyFromDirectory() {
		List<Path> listOfFilePath = DirectoryLister.listRecursivellyFilesAtDirectory(getDefaultPath());
		
		listOfFilePath = listOfFilePath.stream().filter(f -> f.getFileName().toString().contains(".dcm")).collect(Collectors.toList());
		
		Object expected = true;
		Object actual  = listOfFilePath.size() > 10;
		assertEquals(expected, actual);
	}

}
