package br.com.rdsl.infomed.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Queue;

import br.com.rdsl.InfoMed.Utils.Server;

public class RandomReceiverSelector {
	
	List<Server> listofServers = new ArrayList<>();
	Queue<Server> queue = new LinkedList<>();

	public RandomReceiverSelector(Properties props) {

		String DICOM_SERVERS = props.getProperty("DICOM_SERVERS");
		if(DICOM_SERVERS == null || DICOM_SERVERS.split(" ").length == 0) System.err.println("DICOM_SERVER Properties not valid");
		String[] listOfServerFromProperties = DICOM_SERVERS.split(" ");
		for (String server : listOfServerFromProperties) {
			String[] tuple = server.split(":");
			listofServers.add(new Server("ORTHANC" ,tuple[0], tuple[1]));
		}
//		listofServers.add(new Server("ORTHANC", "10.25.133.36", "4242"));
//		listofServers.add(new Server("ORTHANC", "10.25.133.37", "4242"));
//		listofServers.add(new Server("ORTHANC", "10.25.133.38", "4242"));
//		listofServers.add(new Server("ORTHANC", "10.25.133.39", "4242"));
		queue.addAll(this.listofServers);
		
	}
	public  Server getRamdomServer() {
		
		return null;	
	}
	public List<Server> getListOfServers() {
		return this.listofServers;
	}
	public Server getServer() {
			
		Server el = queue.poll();
		if(el == null ) {
			queue.addAll(this.listofServers);
			el = queue.poll();
		}
		
		return el;
	}
}
