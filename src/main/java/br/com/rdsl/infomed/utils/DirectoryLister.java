package br.com.rdsl.infomed.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DirectoryLister {
	public static List<Path> listFilesAtDirectory(String path) {
		List<Path> result = new ArrayList<>();
		try {

			DirectoryStream<Path> ret = Files.newDirectoryStream(Paths.get(path));
			for (Path dsp : ret) {
				result.add(dsp);
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
		return result;
	}

	public static List<File> listOfFolders(String path) {
		
        File[] files = new File(path).listFiles(File::isDirectory);

		return Arrays.asList(files);
	}

	public static List<Path> listRecursivellyFilesAtDirectory(String path) {
		List<Path> listOfFilePaths = new ArrayList<>();
		try {

			listOfFilePaths = Files.walk(Paths.get(path)).filter(Files::isRegularFile).collect(Collectors.toList());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listOfFilePaths;
	}
}
