package br.com.rdsl.infomed.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.stream.Collectors;

import br.com.rdsl.InfoMed.DicomServices.DicomModify;
import br.com.rdsl.InfoMed.DicomServices.DicomSend;
import br.com.rdsl.InfoMed.Utils.OSValidator;
import br.com.rdsl.InfoMed.Utils.Server;
import br.com.rdsl.infomed.Services.Logger;
import br.com.rdsl.infomed.utils.DirectoryLister;
import br.com.rdsl.infomed.utils.RandomReceiverSelector;

public class App {
	public static void main(String[] args) {
		System.out.println("App Runnig");
		
		Properties props = new Properties();
		
		try {
			BufferedReader reader = Files.newBufferedReader(Paths.get("RoboDicom.properties"));
			props.load(reader);
			
		} catch (IOException e1) {
			System.err.println("Problema na leitura do Arquivo de Properties: " + e1.getMessage());
		}
		String BASE_FOLDER = props.getProperty("BASE_FOLDER");
		String ID_UNIDADES = props.getProperty("ID_UNIDADES");
		
		Random randomizer = new Random();
		RandomReceiverSelector ramdomServer = new RandomReceiverSelector(props);
		List<Server> listOfServers = ramdomServer.getListOfServers();
		
		Server server = listOfServers.get(randomizer.nextInt(listOfServers.size()));
		
		String[] arrIdUnidades = ID_UNIDADES.split(" ");
		String idUnidade = null;
		for (String idUnidadesLookup : arrIdUnidades) {
			if(idUnidadesLookup.indexOf(server.getAddress()) > -1) {
				 idUnidade = idUnidadesLookup.split("=")[1];
			}
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(BASE_FOLDER);
		//sb.append(File.separator);
		//sb.append(idUnidade);
		List<File> listOfSubFolders_Modalities = DirectoryLister.listOfFolders(sb.toString());
		
		File selected_modality_Path = listOfSubFolders_Modalities.get(randomizer.nextInt(listOfSubFolders_Modalities.size()));
		
		List<File> listOfSubFolders_ANS = DirectoryLister.listOfFolders(selected_modality_Path.toString());
		
		File selected_AN_PATH = listOfSubFolders_ANS.get(randomizer.nextInt(listOfSubFolders_ANS.size()));
		
		//char sep = File.separatorChar;
		String pathSeparator = File.separator;
		
		if(OSValidator.isWindows()) pathSeparator = pathSeparator + pathSeparator;
		
		String[] ANSelectedArr = selected_AN_PATH.toString().split(pathSeparator);
		
		String ANSelected = ANSelectedArr[ANSelectedArr.length -1 ];

		String[] ModalitySelectedArr = selected_modality_Path.toString().split(pathSeparator);
		
		String ModalitySelected = ModalitySelectedArr[ModalitySelectedArr.length -1 ];
		
		Logger logger = new Logger(new String[] {ModalitySelected, ANSelected}) ;
		
		//selected =  new File("/Users/rodrigo.martins/Documents/DICOMData/636/CR/0000640001779550");
		
		List<Path> listOfFilePath = DirectoryLister.listRecursivellyFilesAtDirectory(selected_AN_PATH.toString());
		
		//List<Path> listOfFilePath = DirectoryLister.listRecursivellyFilesAtDirectory(BASE_FOLDER);
		
		listOfFilePath = listOfFilePath.stream()
				.filter(f -> f.getFileName().toString().contains(".dcm"))
				.filter(f -> f.toString().contains("changed") == false)
				.collect(Collectors.toList());
		
		Map<String, Map<String,Object>> pathTracker = new HashMap<>();
		
		//listOfFilePath.clear();

		//listOfFilePath.add(new File("/Users/rodrigo.martins/Documents/DICOMData/636/US/0013023600000861/1.3.840.20160827.1636.20013023600000861.2314424247/1.3.840.20160827.1636.20013023600000861.2314424247.5.dcm").toPath());
		//listOfFilePath.add(new File("/Users/rodrigo.martins/Documents/DICOMData/636/US/0013023600000861/1.3.840.20160827.1636.20013023600000861.2314424247/1.3.840.20160827.1636.20013023600000861.2314424247.11.dcm").toPath());
		//listOfFilePath.add(new File("/Users/rodrigo.martins/Documents/DICOMData/636/US/0013023600000861/1.3.840.20160827.1636.20013023600000861.2314424247/1.3.840.20160827.1636.20013023600000861.2314424247.8.dcm").toPath());
		//listOfFilePath.add(new File("/Users/rodrigo.martins/Documents/DICOMData/636/US/0013023600000861/1.3.840.20160827.1636.20013023600000861.2314424247/1.3.840.20160827.1636.20013023600000861.2314424247.9.dcm").toPath());
		
		System.out.println("Server Selected: " + server + " AN Selected: " + ANSelected);
		
		String dateNow = new SimpleDateFormat("YYYYMMdd").format(new Date());
		String timeNow = new SimpleDateFormat("HHmmss").format(new Date());
		
		for (Path path : listOfFilePath) {
			Path diretory = path.getParent();
			String filePathAbsolute = path.toString();
			
			System.out.println(server.getAddress() + ":"+ server.getPort() +  " " + path);
			
			//FileLogger.writeINFO(path.toString());
			
			logger.appendLog(path.toString());
			// Controler qnd a pasta já foi processada por um primeiro arquivo
			if(!pathTracker.containsKey(diretory.toString())) { 
				
				try {
					
					Map<String, Object> changesToBeApplied = DicomModify.getChanges(filePathAbsolute , idUnidade , dateNow, timeNow ,props);
					File changedFile = DicomModify.applyChanges(filePathAbsolute, changesToBeApplied, props);
					pathTracker.put(diretory.toString(), changesToBeApplied);
					
					try{
						DicomSend dicomSend = new DicomSend(props);
						dicomSend.send(changedFile.toString(), server);
					}catch (Exception e) {
						// TODO: handle exception
					}
					
				} catch (Exception e) {
					
					System.err.println("Error: " + e.getMessage());
					//FileLogger.writeWARN("Error: " + e.getMessage());
					logger.appendLog("Error: " + e.getMessage());
					
				}
			}else {
				
				try {
					
					Map<String, Object> changesToBeApplied = pathTracker.get(diretory.toString());
					File changedFile = DicomModify.applyChanges(filePathAbsolute, changesToBeApplied, props);
					
					try{
						DicomSend dicomSend = new DicomSend(props);
						dicomSend.send(changedFile.toString(), server);
					}catch (Exception e) {
						// TODO: handle exception
					}
					
				} catch (Exception e) {
					
					System.err.println("Error: " + e.getMessage());
					//FileLogger.writeWARN("Error: " + e.getMessage());
					logger.appendLog("Error: " + e.getMessage());
					
				}	
			}
		}
		
		logger.WriteRunAtFile();
	}
}
