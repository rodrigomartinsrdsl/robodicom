package br.com.rdsl.infomed.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import br.com.rdsl.InfoMed.DicomServices.DicomSend;
import br.com.rdsl.InfoMed.Utils.Server;
import br.com.rdsl.infomed.utils.DirectoryLister;

public class SendFolder {
	static int count = 1;
	
	
	static DicomSend dicomSend =null;
	
	public static void main(String[] args) {
		List<Path> listOfFilePath = DirectoryLister.listRecursivellyFilesAtDirectory("/tmp/DADOS_VIVALE_OSIRIX/");
		
		listOfFilePath = listOfFilePath.stream()
				.filter(f -> f.getFileName().toString().contains(".dcm"))
				.collect(Collectors.toList());
		System.out.println(listOfFilePath.size());
		//System.out.println(listOfFilePath);
		
		Properties props = new Properties();
		
		try {
			BufferedReader reader = Files.newBufferedReader(Paths.get("RoboDicom.properties"));
			props.load(reader);
			
		} catch (IOException e1) {
			System.err.println("Problema na leitura do Arquivo de Properties: " + e1.getMessage());
		}
		
		dicomSend = new DicomSend(props);
		
//		for (Path path : listOfFilePath) {
//			count = SendFileToServer(props, path);
//		}
		
		
		listOfFilePath.parallelStream().forEach(p -> {
			 SendFileToServer(props, p);
		});
	}

	private static int SendFileToServer(Properties props, Path path) {
		try{
			
			System.out.println(count++ + " :Sending " + path.toString() + " to " + "10.25.136.43");
			
			dicomSend.send(path.toString(), new Server("ORTHANC", "10.25.136.43", "4242"));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
}
