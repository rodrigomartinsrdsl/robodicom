package br.com.rdsl.infomed.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.com.rdsl.InfoMed.CommonServices.WorkListService;
import br.com.rdsl.InfoMed.DicomModel.WorkList;
import br.com.rdsl.InfoMed.DicomServices.DicomModify;
import br.com.rdsl.InfoMed.DicomServices.DicomSend;
import br.com.rdsl.InfoMed.Reflection.Intronspector;
import br.com.rdsl.InfoMed.Utils.OSValidator;
import br.com.rdsl.InfoMed.Utils.Server;
import br.com.rdsl.infomed.Services.Logger;
import br.com.rdsl.infomed.utils.DirectoryLister;

public class AppQueryWorklistViaRest {
	private static String SERVER_HOST = "";
	private static String SERVER_PORT = "";
	private static String REST_ENDPOINT = "";
	private static String CODIGO_UNIDADE = "";
	private static Logger logger = null;
	private static String BASE_PATH = "";
	private static String DICOM_SERVER = "";
	private static String DICOM_PORT = "";
	private static long startTime = System.nanoTime();
	private static Properties props = new Properties();
	
	public static void main(String[] args) {

		try {
			BufferedReader reader = Files.newBufferedReader(Paths.get("RoboDicom.properties"));
			props.load(reader);

		} catch (IOException e1) {
			System.err.println("Problema na leitura do Arquivo de Properties: " + e1.getMessage());
		}

		// Get list of files at File system
		// String BASE_PATH = "/Volumes/c$/BaseExamesDicom/";
		BASE_PATH = props.getProperty("BASE_FOLDER");
		SERVER_HOST = props.getProperty("WORKLIST_SERVER");
		SERVER_PORT = props.getProperty("WORKLIST_PORT");
		REST_ENDPOINT = props.getProperty("WORKLIST_ENDPOINT");
		CODIGO_UNIDADE = props.getProperty("WORKLIST_COD_UNIDADE");
		
		DICOM_SERVER = props.getProperty("DICOM_SERVERS").split(":")[0];
		DICOM_PORT = props.getProperty("DICOM_SERVERS").split(":")[1];

		try {
			
			boolean isBASEPATHExists = new File(BASE_PATH).exists();
			if(isBASEPATHExists == false) {
				System.err.println("BASE_PATH does not exists " + BASE_PATH + " please check !");
				System.exit(500);
			}
			
			HttpResponse<JsonNode> ret = Unirest
					.get("http://" + SERVER_HOST + ":" + SERVER_PORT + "/" + REST_ENDPOINT + "/" + CODIGO_UNIDADE)
					.asJson();

			List<WorkList> arrOfRestWorkLists = WorkListService.parseWorkListJsonToObj(ret.getRawBody());

			List<File> listOfModalistsFolders = DirectoryLister.listOfFolders(BASE_PATH);
			Map<String, String> MapOfModalistsFolders = processListAndPutTailPathItemAsKey(listOfModalistsFolders);

			// runForAllWorkListItems(arrOfRestWorkLists, BASE_PATH,
			// MapOfModalistsFolders);

			runForFirstTopWorkListItem(arrOfRestWorkLists, BASE_PATH, MapOfModalistsFolders);

		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			System.err.println("Erro: Problema na Chamada WorkList Rest: http://" + SERVER_HOST + ":" + SERVER_PORT + "/" + REST_ENDPOINT + "/" + CODIGO_UNIDADE + " -> " + e.getMessage());
		}
	}

	private static void runForFirstTopWorkListItem(List<WorkList> arrOfRestWorkLists, String BASE_PATH,
			Map<String, String> mapOfModalistsFolders) {
		
		WorkList selectecWorkListItem = arrOfRestWorkLists.get(new Random().nextInt(arrOfRestWorkLists.size()));
		// WorkList selectecWorkListItem = arrOfRestWorkLists.get(16);

		String selectedModalityPath = getModalityStudiesPathBasedOnModalityName(mapOfModalistsFolders,
				selectecWorkListItem.getModalidade());

		logger = new Logger(new String[] {"Modalidade"});

		logger.enableConsoleDebug();

		logger.appendLog("BASE PATH: " + BASE_PATH);

		logger.appendLog("DICOM SERVER: " + DICOM_SERVER + ":" + DICOM_PORT);

		logger.appendLog("Selected Folder: " + selectedModalityPath);

		runForAllWorkListItems(Arrays.asList(selectecWorkListItem), BASE_PATH, mapOfModalistsFolders);

		logger.appendLog("Time Elapsed: " + (System.nanoTime() - startTime) / 1000000 + "ms");

		logger.WriteRunAtFile();

	}

	private static void runForAllWorkListItems(List<WorkList> arrOfRestWorkLists, String BASE_PATH,
			Map<String, String> MapOfModalistsFolders) {
		// Get list of WorkList Per WL Server
		arrOfRestWorkLists.forEach(wl -> {
			// System.out.println(wl.getProcedimento().getNome());
			// System.out.println(wl.getModalidade());
			// System.out.println(wl.getUnidade().idUnidade);
			// System.out.println(wl.getStudyInstanceUID());
			String MODALIDADE = wl.getModalidade().toLowerCase();
			String DESCRICAO_PROCEDIMENTO = wl.getProcedimento().getNome().toUpperCase();
			String selectedModalityPath = getModalityStudiesPathBasedOnModalityName(MapOfModalistsFolders, MODALIDADE);

			List<Path> studyLevelFiles = processModalityLevel(BASE_PATH, MODALIDADE, DESCRICAO_PROCEDIMENTO,
					selectedModalityPath);
			
			//List<Path> studyLevelFiles = returnRecursiveListOfFilesWithoutChangeFolder(new File("/Volumes/c$/BaseExamesDicom/CT/TORAX/0000640001616545"));
			DicomSend dicomSend = new DicomSend();
			AtomicInteger counter = new AtomicInteger();
			studyLevelFiles.forEach(dicomFilePath -> {
				try {
					String dateNow = new SimpleDateFormat("YYYYMMdd").format(new Date());
					String timeNow = new SimpleDateFormat("HHmmss").format(new Date());
					DicomModify dicomModify = new DicomModify();
					int counterInt = counter.incrementAndGet();
					
					String[] StudyInstanceUIDCharArray = wl.getStudyInstanceUID().split("");
					
					StudyInstanceUIDCharArray = Arrays.stream(StudyInstanceUIDCharArray).filter(l -> {
						Character c = l.charAt(0);
						int hascode = c.hashCode();
						if(hascode > 0) { // Retira caracteres em nulos ou invalidos da string
							return true;
						}
						return false;
						
					}).toArray(String[]::new);
					
					String StudyInstanceUIDCleanedWithNotNull = String.join("", StudyInstanceUIDCharArray);
					
					if(counterInt == 1) {
						logger.appendLog("AN: " + wl.getAN());
						logger.appendLog("Paciente: " + wl.getPaciente().nome);
						logger.appendLog("Modalidade: " + wl.getModalidade());
						logger.appendLog("Descrição Exame: " + wl.getProcedimento().getNome());
						logger.appendLog("StudyInstanceUID: " + StudyInstanceUIDCleanedWithNotNull);
					}
					
					dicomModify.setInstitutionName("Hospital " + wl.getUnidade().idUnidade);
					
					dicomModify.setSeriesTime(timeNow);
					dicomModify.setStudyTime(timeNow);

					dicomModify.setStudyDate(dateNow);
					dicomModify.setSeriesDate(dateNow);
					
					dicomModify.setPatientName(wl.getPaciente().nome);
					dicomModify.setPatientID(wl.getPaciente().codigo);
					
					dicomModify.setPatientBirthDate(wl.getPaciente().dataNascimento);
					dicomModify.setPatientSex(wl.getPaciente().sexo);
					
					dicomModify.setStudyInstanceUID(StudyInstanceUIDCleanedWithNotNull);
					 
					dicomModify.setRequestingPhysician(wl.getSolicitante().nome);
					
					dicomModify.setStudyDescription(wl.getProcedimento().getNome());
					dicomModify.setAccessionNumber(wl.getAN());
					
					Map<String, Object> changesToBeApplied = Intronspector.getNotNullFiels(dicomModify);
					System.out.println("Applying Changes");
					
					File changedFile = DicomModify.applyChangesWithoutParseDicomFile(dicomFilePath.toString(), changesToBeApplied, props);
					
					System.out.println("Sending Dicom ...");
					
					dicomSend.send(changedFile.toString(), new Server("ORTHANC", DICOM_SERVER, DICOM_PORT));
					
					logger.appendLog("ENVIADO(" + counterInt + "): " + changedFile.toString());
				} catch (Exception e) {
					logger.appendLog("Dicom Send Error:" + e.toString());
				}
			});

			logger.appendLog("Encontrado " + studyLevelFiles.size() + " dicom Files");

		});
	}

	private static String getModalityStudiesPathBasedOnModalityName(Map<String, String> MapOfModalistsFolders,
			String MODALIDADE) {
		String selectedModalityPath = MapOfModalistsFolders.get(MODALIDADE.toLowerCase());
		return selectedModalityPath;
	}

	private static List<Path> processModalityLevel(String BASE_PATH, String MODALIDADE, String DESCRICAO_PROCEDIMENTO,
			String selectedModalityPath) {
		List<Path> studyLevelFiles = new ArrayList<>();
		
		if (selectedModalityPath != null) {

			List<File> listOfAreasDoCorpo = DirectoryLister.listOfFolders(selectedModalityPath);

			studyLevelFiles = processAreaExameLevel(DESCRICAO_PROCEDIMENTO, selectedModalityPath, listOfAreasDoCorpo);

		} else {
			logger.appendLog(MODALIDADE + " folder nao encontrada na pasta " + BASE_PATH);
		}
		return studyLevelFiles;
	}

	private static List<Path> processAreaExameLevel(String DESCRICAO_PROCEDIMENTO, String selectedModalityPath,
			List<File> listOfAreasDoCorpo) {
		Map<String, String> MapOfAreasExameFolders = processListAndPutTailPathItemAsKey(listOfAreasDoCorpo);
		Set<String> areas = MapOfAreasExameFolders.keySet();
		Optional<String> found = areas.stream().filter(a -> {
			// System.out.println(a.toString());
			return DESCRICAO_PROCEDIMENTO.contains(a.toString().toUpperCase());
		}).findFirst();

		List<Path> studyLevelFiles = new ArrayList<>();
		if (found.isPresent()) {
			// System.out.println(found.get());

			String AreaExamePath = MapOfAreasExameFolders.get(found.get());
			logger.appendLog("Selected Area Exame:" + AreaExamePath + " for Description: " + DESCRICAO_PROCEDIMENTO);

			studyLevelFiles = processANLevel(AreaExamePath);

		} else {
			logger.appendLog(DESCRICAO_PROCEDIMENTO + " nao encontrado em " + selectedModalityPath);
		}

		return studyLevelFiles;
	}

	private static List<Path> processANLevel(String AreaExamePath) {
		List<File> listOfANs = DirectoryLister.listOfFolders(AreaExamePath);
		Random randomizer = new Random();
		File randomANFolder = listOfANs.get(randomizer.nextInt(listOfANs.size()));

		logger.appendLog("Selected AN:" + randomANFolder);
		List<Path> studyLevelFiles = returnRecursiveListOfFilesWithoutChangeFolder(randomANFolder);
		return studyLevelFiles;
	}

	private static List<Path> returnRecursiveListOfFilesWithoutChangeFolder(File randomANFolder) {
		List<Path> studyLevelFiles = DirectoryLister.listRecursivellyFilesAtDirectory(randomANFolder.toString());

		studyLevelFiles = studyLevelFiles.stream().filter(folderName -> {
			return folderName.toString().contains("changed") == false;
		}).collect(Collectors.toList());
		return studyLevelFiles;
	}

	private static Map<String, String> processListAndPutTailPathItemAsKey(List<File> listOfModalistsFolders) {
		Map<String, String> map = new HashMap<>();
		listOfModalistsFolders.forEach(folder -> {
			String separator = File.separator;
			if(OSValidator.isWindows()) separator += separator;
			String[] folderNameArr = folder.toString().split(separator);
			String modalityName = folderNameArr[folderNameArr.length - 1];
			// System.out.println(modalityName);
			if (modalityName != null)
				map.put(modalityName.toLowerCase(), folder.toString());
		});
		return map;
	}

}
