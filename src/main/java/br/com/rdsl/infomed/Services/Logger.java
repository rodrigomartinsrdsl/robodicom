package br.com.rdsl.infomed.Services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	private  final String LOG_PATH =  "logs" + File.separator;
	
	private  StringBuilder content;

	private String fileName;
	private Boolean debugAtConsole = false;
	
	public Logger(String args[]) {
		String dateNow = new SimpleDateFormat("YYYYMMdd").format(new Date());
		String timeNow = new SimpleDateFormat("HHmmss").format(new Date());
		this.content =  new StringBuilder();
		
		for (int i = 0; i < args.length; i++) {
			String separator = "";
			if(i <  args.length - 1 ) separator = "_";
			this.content.append(args[i] + separator);
		}
		if(this.content.toString().isEmpty() == false) this.content.append("-");
		Path currentDir = Paths.get("");
		new File(currentDir.toAbsolutePath() + File.separator +   LOG_PATH ).mkdir();
		this.fileName =  currentDir.toAbsolutePath() + File.separator + LOG_PATH + this.content.toString() +  dateNow+ "_" + timeNow + ".log";
		this.content = new StringBuilder();
	}
	public void enableConsoleDebug() {
		this.debugAtConsole = true;
	}
	public  void appendLog(String content) {
		if(this.debugAtConsole) System.out.println(content);
		this.content.append(content +"\n");
	}
	
	public  void WriteRunAtFile() {
		Writer writer = null;

		try {
		    writer = new BufferedWriter(new FileWriter(this.fileName));
		    writer.write(this.content.toString());
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
		   try {writer.close();} catch (Exception ex) {/*ignore*/}
		}
	}

}
